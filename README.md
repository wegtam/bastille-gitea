# Gitea template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Gitea](https://gitea.io/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

The following arguments **must be provided** as a bare minimum:

- DB_PASS
- DOMAIN
- EXT_BACKUP_DIR
- EXT_CUSTOM_DIR
- EXT_DB_DIR
- EXT_LOG_DIR
- INTERNAL_TOKEN
- JWT_SECRET
- LFS_JWT_SECRET
- ROOT_URL
- SECRET_KEY

The idea of using the external directories is to have your gitea
configuration, data, logs and customisation outside of the jail for easier
backup and prevention of data loss upon jail re-creation.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

```
# bastille bootstrap https://codeberg.org/wegtam/bastille-gitea
```

## Usage

```
# bastille template TARGET wegtam/bastille-gitea --arg DB_PASS=XXX \
  --arg INTERNAL_TOKEN=XXX --arg JWT_SECRET=XXX --arg LFS_JWT_SECRET=XXX \
  --arg SECRET_KEY=XXX --arg EXT_DB_DIR=/srv/gitea-data \
  --arg EXT_CUSTOM_DIR=/srv/gitea-custom --arg EXT_LOG_DIR=/var/log/gitea-jail \
  --arg EXT_BACKUP_DIR=/srv/gitea-backups \
  --arg DOMAIN=git.example.com --arg ROOT_URL=https://git.example.com
```

For more options take a look at the [Bastillefile](Bastillefile).

```
# bastille template TARGET wegtam/bastille-gitea --arg APP_NAME="My Gitea" \
  --arg DB_PASS=XXX --arg DB_HOST=10.8.0.10:5432 --arg DB_SSL=enable \
  --arg INTERNAL_TOKEN=XXX --arg JWT_SECRET=XXX --arg LFS_JWT_SECRET=XXX \
  --arg SECRET_KEY=XXX --arg DOMAIN=git.example.com
  --arg EXT_CUSTOM_DIR=/srv/gitea-custom --arg EXT_LOG_DIR=/var/log/gitea-jail \
  --arg EXT_BACKUP_DIR=/srv/gitea-backups \
  --arg EMAIL_ENABLED=true --arg EMAIL_HOST=localhost:465 \
  --arg EMAIL_USER=XXX --arg EMAIL_PASS=XXX --arg EMAIL_TLS=true \
  --arg EMAIL_NOTIFY=true \
  --arg DOMAIN=git.example.com --arg ROOT_URL=https://git.example.com
```

